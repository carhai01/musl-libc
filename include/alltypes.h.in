#define __LITTLE_ENDIAN 1234
#define __BIG_ENDIAN 4321
#define __USE_TIME_BITS64 1

TYPEDEF unsigned _Addr size_t;
TYPEDEF __UINTPTR_TYPE__ uintptr_t;
TYPEDEF _Addr ptrdiff_t;
TYPEDEF _Addr ssize_t;
TYPEDEF __INTPTR_TYPE__ intptr_t;
TYPEDEF _Addr regoff_t;
TYPEDEF _Reg register_t;
TYPEDEF _Int64 time_t;
TYPEDEF _Int64 suseconds_t;

TYPEDEF signed char     int8_t;
TYPEDEF signed short    int16_t;
TYPEDEF signed int      int32_t;
TYPEDEF signed _Int64   int64_t;
TYPEDEF signed _Int64   intmax_t;
TYPEDEF unsigned char   uint8_t;
TYPEDEF unsigned short  uint16_t;
TYPEDEF unsigned int    uint32_t;
TYPEDEF unsigned _Int64 uint64_t;
TYPEDEF unsigned _Int64 u_int64_t;
TYPEDEF unsigned _Int64 uintmax_t;

TYPEDEF unsigned mode_t;
TYPEDEF unsigned _Reg nlink_t;
TYPEDEF _Int64 off_t;
TYPEDEF unsigned _Int64 ino_t;
TYPEDEF unsigned _Int64 dev_t;
TYPEDEF long blksize_t;
TYPEDEF _Int64 blkcnt_t;
TYPEDEF unsigned _Int64 fsblkcnt_t;
TYPEDEF unsigned _Int64 fsfilcnt_t;

TYPEDEF unsigned wint_t;
TYPEDEF unsigned long wctype_t;

#ifdef __CHERI_PURE_CAPABILITY__
#ifdef __timer_t_defined
#undef __timer_t_defined
#endif
#ifndef __cheri_timer_t
#define __cheri_timer_t
typedef struct timer_t
{
    void *ptr;
    char thread;
} timer_t;
#endif
#else
TYPEDEF void * timer_t;
#endif

TYPEDEF int clockid_t;
TYPEDEF long clock_t;
STRUCT timeval { time_t tv_sec; suseconds_t tv_usec; };
STRUCT timespec { time_t tv_sec; int :8*(sizeof(time_t)-sizeof(long))*(__BYTE_ORDER==4321); long tv_nsec; int :8*(sizeof(time_t)-sizeof(long))*(__BYTE_ORDER!=4321); };

TYPEDEF int pid_t;
TYPEDEF unsigned id_t;
TYPEDEF unsigned uid_t;
TYPEDEF unsigned gid_t;
TYPEDEF int key_t;
TYPEDEF unsigned useconds_t;

#ifdef __cplusplus
TYPEDEF __UINTPTR_TYPE__ pthread_t;
#else
TYPEDEF struct __pthread * pthread_t;
#endif
TYPEDEF int pthread_once_t;
TYPEDEF unsigned pthread_key_t;
TYPEDEF int pthread_spinlock_t;
TYPEDEF struct { unsigned __attr; } pthread_mutexattr_t;
TYPEDEF struct { unsigned __attr; } pthread_condattr_t;
TYPEDEF struct { unsigned __attr; } pthread_barrierattr_t;
TYPEDEF struct { unsigned __attr[2]; } pthread_rwlockattr_t;

STRUCT _IO_FILE { char __x; };
TYPEDEF struct _IO_FILE FILE;

TYPEDEF __builtin_va_list va_list;
TYPEDEF __builtin_va_list __isoc_va_list;

TYPEDEF struct __mbstate_t { unsigned __opaque1, __opaque2; } mbstate_t;

TYPEDEF struct __locale_struct * locale_t;

TYPEDEF struct __sigset_t { unsigned long __bits[128/sizeof(long)]; } sigset_t;

STRUCT iovec { void *iov_base; size_t iov_len; };

STRUCT winsize { unsigned short ws_row, ws_col, ws_xpixel, ws_ypixel; };

TYPEDEF unsigned socklen_t;
TYPEDEF unsigned short sa_family_t;

#if defined(__CHERI__)
typedef __intcap_t intcap_t;
typedef __uintcap_t uintcap_t;
typedef __PTRADDR_TYPE__ ptraddr_t;
#else
typedef unsigned _Addr ptraddr_t;
#endif

#ifdef __CHERI_PURE_CAPABILITY__
#define ATTR_LEN 16
#define MUTEX_LEN 16
#define COND_LEN 16
#define RWLOCK_LEN 16
#define BARRIER_LEN 16
#else
#define ATTR_LEN sizeof(long)==8?14:9
#define MUTEX_LEN sizeof(long)==8?10:6
#define COND_LEN 12
#define RWLOCK_LEN sizeof(long)==8?14:8
#define BARRIER_LEN 8?8:5
#endif

TYPEDEF struct { union { int __i[ATTR_LEN]; volatile int __vi[ATTR_LEN]; unsigned long __s[ATTR_LEN*sizeof(int)/sizeof(long)]; void *__p[ATTR_LEN*sizeof(int)/sizeof(void*)]; } __u; } pthread_attr_t;
TYPEDEF struct { union { int __i[MUTEX_LEN]; volatile int __vi[MUTEX_LEN]; volatile void *volatile __p[MUTEX_LEN*sizeof(int)/sizeof(void*)]; } __u; } pthread_mutex_t;
TYPEDEF struct { union { int __i[MUTEX_LEN]; volatile int __vi[MUTEX_LEN]; volatile void *volatile __p[MUTEX_LEN*sizeof(int)/sizeof(void*)]; } __u; } mtx_t;
TYPEDEF struct { union { int __i[COND_LEN]; volatile int __vi[COND_LEN]; void *__p[COND_LEN*sizeof(int)/sizeof(void*)]; } __u; } pthread_cond_t;
TYPEDEF struct { union { int __i[COND_LEN]; volatile int __vi[COND_LEN]; void *__p[COND_LEN*sizeof(int)/sizeof(void*)]; } __u; } cnd_t;
TYPEDEF struct { union { int __i[RWLOCK_LEN]; volatile int __vi[RWLOCK_LEN]; void *__p[RWLOCK_LEN*sizeof(int)/sizeof(void*)]; } __u; } pthread_rwlock_t;
TYPEDEF struct { union { int __i[BARRIER_LEN]; volatile int __vi[BARRIER_LEN]; void *__p[BARRIER_LEN*sizeof(int)/sizeof(void*)]; } __u; } pthread_barrier_t;

#undef _Addr
#undef _Int64
#undef _Reg
